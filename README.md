![](logo.png)
## Requirements
- A Unix like operating System (e.g. Linux)
- A Unicode supporting mono spaced font

## Installation
Clone the repository and run via cargo:
```
git clone https://codeberg.org/B4rc1/snake.git
cargo run
```

## Key bindings
Vim keys and `q` to quit at any time.

```
         ↑
         k
     ← h   l →
         j
         ↓
```

## Which font and colorscheme is that in the screenshot?
- Font: [Cozette](https://github.com/slavfox/Cozette)
- colorscheme: [iceberg.vim](https://cocopon.github.io/iceberg.vim/) (colors extracted from vim theme)

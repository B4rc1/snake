extern crate rand;
extern crate termion;

use termion::async_stdin;
use termion::raw::IntoRawMode;
use termion::screen::*;
use termion::terminal_size;

use std::io::{stdout, Read, Write};
use std::{thread, time};

use rand::Rng;

// ═╗
//  ║ ╔☺ 
//  ╚═╝

trait Tile {
    fn symbol(&self) -> &str;
}

#[derive(Clone, Debug)]
enum PlayerTile {
    Head,
    Horizontal,
    Vertical,
    SouthEast,
    SouthWest,
    NorthEast,
    NorthWest,
    ERROR,
}

// ═ ║ ╔ ╗ ╚ ╝ ☺
impl Tile for PlayerTile {
    fn symbol<'a>(&'a self) -> &'a str {
        match self {
            &PlayerTile::Head => "☺",
            &PlayerTile::Horizontal => "║",
            &PlayerTile::Vertical => "═",
            &PlayerTile::SouthEast => "╔",
            &PlayerTile::SouthWest => "╗",
            &PlayerTile::NorthEast => "╚",
            &PlayerTile::NorthWest => "╝",
            &PlayerTile::ERROR => "?",
        }
    }
}

#[derive(Clone, Debug)]
enum GameTile {
    Empty,
    Wall,
    Player(PlayerTile),
    Fruit,
}

impl PartialEq for GameTile {
    fn eq(&self, other: &GameTile) -> bool {
        use std::mem::discriminant;
        // We don't care about the specific 'PlayerTile' if it's a 'Player' GameTile, just the fact, that it *is* a
        // 'Player' Tile
        discriminant(self) == discriminant(other)
    }
}

impl Tile for GameTile {
    fn symbol<'a>(&'a self) -> &'a str {
        match self {
            &GameTile::Empty => " ",
            &GameTile::Wall => "█",
            &GameTile::Fruit => "",
            GameTile::Player(player_tile) => player_tile.symbol(),
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
struct BodyPart {
    x: usize,
    y: usize,
    direction: Direction,
}

impl BodyPart {
    fn get_index(&self, width: usize) -> usize {
        self.x + self.y * width
    }

    fn transition_to_dir(&mut self, target_dir: &Direction) -> PlayerTile {
        use Direction::*;
        let tile = match (self.direction, target_dir) {
            (Up, Up)       | (Down, Down)  => PlayerTile::Horizontal,
            (Right, Right) | (Left, Left)  => PlayerTile::Vertical,
            (Up, Right)    | (Left, Down)  => PlayerTile::SouthEast,
            (Right, Down)  | (Up, Left)    => PlayerTile::SouthWest,
            (Left, Up)     | (Down, Right) => PlayerTile::NorthEast,
            (Right, Up)    | (Down, Left)  => PlayerTile::NorthWest,
            _                              => PlayerTile::ERROR,
        };

        self.direction = *target_dir;
        tile
    }
}

#[derive(Clone)]
struct Player {
    head: BodyPart,
    body: Vec<BodyPart>,
}

#[derive(Clone, Copy, PartialEq, Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    fn is_oppose_to(&self, dir: &Direction) -> bool {
        match self {
            &Direction::Down => dir == &Direction::Up,
            &Direction::Left => dir == &Direction::Right,
            &Direction::Right => dir == &Direction::Left,
            &Direction::Up => dir == &Direction::Up,
        }
    }
}

struct Board<T: Write> {
    screen: AlternateScreen<T>,
    height: usize,
    width: usize,

    tiles: Vec<GameTile>,
    encoded: Vec<u8>,

    player: Player,
    score: usize,
}

enum GameResult {
    Proceed,
    PlayerHasDied,
}

impl<T> Board<T>
where
    T: Write,
{
    /// Create new board with supplied board height and width.
    /// This takes a closure that is executed for each tile and determines how the initial board will be laid out.
    /// The closure 'create_layout' has the arguments `(x: usize, y: usize,width: usize, height: usize, tile: &mut GameTile)`.
    fn new<C>(screen: AlternateScreen<T>, height: usize, width: usize, mut create_layout: C) -> Self
    where
        C: FnMut(usize, usize, usize, usize, &mut GameTile),
    {
        Board {
            screen,
            height,
            width,
            tiles: {
                let mut tiles = vec![GameTile::Empty; height * width];

                for (tile_no, tile) in tiles.iter_mut().enumerate() {
                    let x = tile_no % width;
                    let y = tile_no / width;

                    (create_layout)(x, y, width, height, tile)
                }
                tiles
            },

            encoded: {
                let mut encoded: Vec<u8> = Vec::new();
                // It will probably be larger, we just don't know how much
                encoded.reserve(width * height);
                encoded
            },

            player: Player {
                head: BodyPart {
                    x: width / 2,
                    y: height / 2,
                    direction: Direction::Right,
                },
                body: Vec::new(),
            },
            score: 0,
        }
    }

    #[must_use]
    fn update_tiles(
        &mut self,
        player_input: Option<Direction>,
        rng: &mut rand::rngs::ThreadRng,
    ) -> GameResult {
        let old_player = self.player.clone();

        // FIXME: There is a race condition here, allowing the player to instantly reverse
        // directions. Has something to do with async_stdin.
        if let Some(player_input) = player_input {
            if player_input != self.player.head.direction
                && !player_input.is_oppose_to(&self.player.head.direction)
            {
                self.player.head.direction = player_input;
            }
        }

        match self.player.head.direction {
            Direction::Right => self.player.head.x += 1,
            Direction::Left => self.player.head.x -= 1,
            Direction::Up => self.player.head.y -= 1, // (0, 0) is the top left corner
            Direction::Down => self.player.head.y += 1,
        }

        // Collision detection
        match &self.tiles[self.player.head.get_index(self.width)] {
            // eat fruit, get bigger, increment score and spawn new fruit
            &GameTile::Fruit => {
                // append new body part
                let last_body_part: &BodyPart;
                match old_player.body.iter().last() {
                    Some(bdy) => last_body_part = bdy,
                    None => last_body_part = &old_player.head,
                }
                self.player.body.push(last_body_part.clone());

                self.score += 100;

                // spawn new fruit with randomized position
                let mut fruit_index: usize;
                // search for an empty spot to grantee that a fruit will spawn
                // FIXME: this will hang forever if the player is as big as the play area.
                //        This should not be a concern as the play is most likely not a human.
                loop {
                    // 1 as lower bound because there is a wall at 0
                    // width/height - 2 because -1 is the wall
                    let fruit_x = rng.gen_range(1..(self.width - 2));
                    let fruit_y = rng.gen_range(1..(self.height - 2));
                    fruit_index = fruit_x + fruit_y * self.width;

                    match self.tiles[fruit_index] {
                        GameTile::Empty => break,
                        _ => continue,
                    }
                }
                self.tiles[fruit_index] = GameTile::Fruit;
            }
            // kill the player if he hits himself or a wall
            &GameTile::Wall | GameTile::Player(_) => return GameResult::PlayerHasDied,
            _ => (),
        }

        // Move the body of the player.
        //
        // Construct a copy of the body array in order to read from it with an offset of one like
        // this:
        //
        // index           :   3     2     1     0
        // body_cloned     : [ ║ ] [ ╔ ] [ ═ ] [ ═ ] :)
        //                        /     /     /     /
        // self.player.body: [ ║ ] [ ╔ ] [ ═ ] [ ═ ]
        // The first element is read from the old head of the player.
        let mut body_cloned = self.player.body.clone();
        let mut body_cloned_iter = body_cloned.iter_mut();
        for (index, bdy) in self.player.body.iter_mut().enumerate() {
            let next_body_part: &BodyPart;
            let dir: Direction;

            if index == 0 {
                next_body_part = &old_player.head;
                // update with new direction of the head immediately in order to make transitions look right
                dir = self.player.head.direction;
            } else {
                next_body_part = body_cloned_iter.next().expect("no previous body"); // This should never fail (see diagram above)
                dir = next_body_part.direction;
            }
            bdy.x = next_body_part.x;
            bdy.y = next_body_part.y;

            let tile = bdy.transition_to_dir(&dir);
            self.tiles[bdy.get_index(self.width)] = GameTile::Player(tile);
        }

        // Clear the left behind tiles
        let clear_tile_index: usize;
        if let Some(last_body_part) = old_player.body.iter().last() {
            clear_tile_index = last_body_part.get_index(self.width);
        } else {
            // There is no last tile, It's just the head
            clear_tile_index = old_player.head.get_index(self.width);
        }
        self.tiles[clear_tile_index] = GameTile::Empty;

        self.tiles[self.player.head.get_index(self.width)] = GameTile::Player(PlayerTile::Head);

        self.score += 1;
        GameResult::Proceed
    }

    /// render the board to the screen
    fn render(&mut self) {
        // Encode the UTF-8 Sequences contained in into the byte array expected by 'write'
        self.encoded.clear();
        for tile in self.tiles.iter() {
            self.encoded.extend(tile.symbol().as_bytes());
        }

        // render self.encoded onto the screen
        self.screen.write_all(&self.encoded).unwrap();
        self.screen.flush().unwrap();
    }

    fn game_over_screen(&mut self) {
        write!(self.screen, "{}{}", ToMainScreen, termion::cursor::Show).unwrap();
        // FIXME: Why does this get offset?
        // println!("╔═════════════════════════════════╗");
        // println!("║ You died with a score of: {:04} ║", self.score);
        // println!("╚═════════════════════════════════╝");
        println!("You died with a score of: {}", self.score);
    }
}

fn main() {
    let mut rng = rand::thread_rng();

    let mut stdin = async_stdin().bytes();

    let mut screen = AlternateScreen::from(stdout().into_raw_mode().unwrap());
    write!(screen, "{}{}", termion::cursor::Hide, ToAlternateScreen).unwrap();
    screen.flush().unwrap();

    let (rows, cols) = terminal_size().unwrap();
    let (rows, cols) = (rows as usize, cols as usize);

    let mut fruit_x: usize;
    let mut fruit_y: usize;
    loop {
        // 1 as lower bound because there is a wall at 0
        // width/height - 2 because -1 is the wall
        fruit_x = rng.gen_range(1..(cols - 2));
        fruit_y = rng.gen_range(1..(cols - 2));

        if (fruit_x, fruit_y) != (rows / 2, cols / 2) {
            break;
        }
    }

    let mut board = Board::new(screen, cols, rows, |x, y, width, height, tile| {
        if x == 0 || y == 0 || x == width - 1 || y == height - 1 {
            *tile = GameTile::Wall;
        }
        if x == fruit_x && y == fruit_y {
            *tile = GameTile::Fruit;
        }

        if x == width / 2 && y == height / 2 {
            *tile = GameTile::Player(PlayerTile::Head);
        }
    });

    // Game Loop
    loop {
        let b = stdin.next();

        // If we have input, parse it
        //
        // TODO: Make this use termion keys instead of this in order to support
        //       arrow keys
        //       like this:
        //       ```match b {
        //              Key::Char('q') => break,
        //              Key::Up    | Key::Char('k') => break,
        //              Key::Down  | Key::Char('j') => break,
        //              Key::Left  | Key::Char('h') => break,
        //              Key::Right | Key::Char('l') => break,
        //              _ => {},
        //       }````
        //       This does not work as expected, as 'async_stdin' return bytes not keys
        //       and I did not (yet) find a way to convert bytes into keys.
        //
        let mut player_input: Option<Direction> = None;
        if let Some(Ok(char)) = b {
            match char {
                b'q' => break,
                b'h' => player_input = Some(Direction::Left),
                b'j' => player_input = Some(Direction::Down),
                b'k' => player_input = Some(Direction::Up),
                b'l' => player_input = Some(Direction::Right),
                _ => (),
            }
        }

        match board.update_tiles(player_input, &mut rng) {
            GameResult::Proceed => (),
            GameResult::PlayerHasDied => {
                board.game_over_screen();
                break;
            }
        }

        board.render();

        let frametime = time::Duration::from_millis(250);
        thread::sleep(frametime);
    }
}
